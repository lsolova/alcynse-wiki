package org.alcynse.wiki.page;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.alcynse.util.Resource;
import org.alcynse.util.ResourceResolver;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.LoggerFactory;

/**
 * This object store and handle all wiki pages discovered.
 * 
 * @author Laszlo Solova
 */
public class PageStore {

  /**
   * Store all page by the special path.
   */
  static Map<String, WikiPage>  pagesByPath  = new HashMap<String, WikiPage>();
  static Map<String, WikiImage> imagesByPath = new HashMap<String, WikiImage>();
  static PageStoreIndexer       indexer;
  /**
   * The singleton instance of this object.
   */
  // private static PageStore instance;

  private PageStore() {}

  // public static PageStore getInstance() {
  // if (instance == null)
  // instance = new PageStore();
  // return instance;
  // }

  /**
   * Initialize page store. The wiki pages are discovered via this
   * initialization process.
   */
  public static void init() {
    List<Resource> foundWikis = ResourceResolver.getResources(
        Pattern.compile(".*\\.wiki"), null);
    byte[] buffer = new byte[1024];
    int read = -1;
    for (Resource fWiki : foundWikis) {
      try {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = ResourceResolver.getResourceStream(fWiki);
        while ((read = is.read(buffer)) != -1)
          baos.write(buffer, 0, read);
        is.close();
        WikiPage resolvedWikiPage = new WikiPage("/" + fWiki.getFilename(),
            fWiki.getSourcePackName(), baos.toString());
        pagesByPath.put(resolvedWikiPage.getLink(), resolvedWikiPage);
        LoggerFactory.getLogger(PageStore.class).info(
            "Wiki page loaded: '" + resolvedWikiPage.getLink() + " | "
                + resolvedWikiPage.getSourceInfo() + "'");
      } catch (Exception e) {
        LoggerFactory.getLogger(PageStore.class).warn(
            "Wiki page loading failed: " + fWiki, e);
      }
    }
    indexer = new PageStoreIndexer(pagesByPath.values());
    indexer.start();
  }
  public static void addImage(WikiImage image) {
    String encodedImageLink = getEncodedWikiLink(image.getSourceName(),
        image.getName());
    if (!imagesByPath.containsKey(encodedImageLink))
      imagesByPath.put(encodedImageLink, image);
  }
  /**
   * Resolve (get) the page by pageinfo.
   * 
   * @param pageInfo The page id pageinfo.
   * @return The page or null if not found.
   */
  public static WikiPage resolvePage(String pageInfo) {
    WikiPage wp = pagesByPath.get(pageInfo);
    if (wp == null && pageInfo.length() == 0) {
      for (String wpName : getWikiPageList()) {
        WikiPage tmpwp = pagesByPath.get(wpName);
        if ("Home".equalsIgnoreCase(tmpwp.getName()))
          wp = tmpwp;
      }
    }
    return wp;
  }
  public static WikiImage getImage(String imageInfo) {
    return imagesByPath.get(imageInfo);
  }
  public static void setDefaultPage(String defaultPage) {
    // TODO
  }
  /**
   * Get the named page.
   * 
   * @param wpName The encoded wiki link.
   * @return The page or null.
   */
  public static WikiPage getWikiPage(String wpName) {
    return pagesByPath.get(wpName);
  }
  /**
   * Get the list of all pages' wiki link.
   * 
   * @return The list of encoded wiki links.
   */
  public static List<String> getWikiPageList() {
    return new ArrayList<String>(pagesByPath.keySet());
  }

  /**
   * Get the encoded form of the wiki link. This encoded form is used as web
   * links.
   * 
   * @param sourceName The source, where the wiki page extracted from, if it was
   *          found in a jar file. It can be null.
   * @param name The page name, with package name and file extension.
   * @return The encoded wiki link.
   */
  public static String getEncodedWikiLink(String sourceName, String name) {
    return new String(Base64.encodeBase64URLSafe(DigestUtils
        .sha1(getRawWikiLink(
            sourceName, name).getBytes())));
  }
  /**
   * Get wiki link, which is used as a fully unique page id.
   * 
   * @param sourceName The source, where the wiki page extracted from, if it was
   *          found in a jar file. It can be null.
   * @param name The page name, with package name and file extension.
   * @return The wiki link.
   */
  public static String getRawWikiLink(String sourceName, String name) {
    return ((sourceName != null) ? sourceName + ":" : "") + name;
  }
  /**
   * This method is designed to resolve a path to a previously selected absolute
   * wiki path.
   * 
   * @param wikiPath The original absolute wiki path.
   * @param resolvePath The path to resolve based on the wikiPath.
   * @return The resolved absolute path.
   */
  public static String resolveToAbsolute(String wikiPath, String resolvePath) {
    if (resolvePath.startsWith("/"))
      return resolvePath;

    List<String> resolvedPathElements = new LinkedList<String>(
        Arrays.asList(wikiPath.split("/")));
    resolvedPathElements.remove(0);
    List<String> resolvePathElements = Arrays.asList(resolvePath.split("/"));

    for (String rpE : resolvePathElements) {
      if (rpE.isEmpty())
        continue;
      if ("..".equals(rpE))
        resolvedPathElements.remove(resolvedPathElements.size() - 1);
      else
        resolvedPathElements.add(rpE);
    }

    StringBuilder sb = new StringBuilder();
    for (String rdpE : resolvedPathElements) {
      sb.append("/").append(rdpE);
    }
    return (sb.length() == 0) ? "/" : sb.toString();
  }

}
