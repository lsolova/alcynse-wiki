package org.alcynse.wiki.page;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;

/**
 * The representation of a wiki page resolved.
 * 
 * @author Laszlo Solova
 */
public class WikiPage {
  protected String         rawContent;
  private transient String wikiContent;
  protected String         name;
  private String           permission;
  private String           keywords;
  private String           sourceName;

  private final Pattern    PERMISSION_PATTERN = Pattern
      .compile("permission\\(\"(\\w*)\"\\)");
  private final Pattern    KEYWORD_PATTERN    = Pattern
      .compile("permission\\(\"([\\w ]*)\"\\)");

  public WikiPage(String name, String sourceName, String content) {
    this.name = name;
    this.sourceName = sourceName;
    while (content.startsWith("@") || content.startsWith("\n")) {
      if (content.startsWith("@")) {
        String header = content.substring(1, content.indexOf("\n"));
        Matcher m = null;
        // Read permission
        m = PERMISSION_PATTERN.matcher(header);
        if (m.matches() && m.group(1) != null) {
          this.permission = m.group(1);
        }
        // Read keywords
        m = KEYWORD_PATTERN.matcher(header);
        if (m.matches() && m.group(1) != null) {
          this.keywords = m.group(1);
        }
      }
      // Remove headers
      content = content.substring(content.indexOf("\n") + 1);
    }
    this.rawContent = content;
  }
  public String getContent() throws Exception {
    if (wikiContent == null) {
      synchronized (this) {
        if (wikiContent == null) {
          wikiContent = PageParser.parse(this);
        }
      }
    }
    return wikiContent;
  }
  String getRawContent() {
    return rawContent;
  }
  public Set<String> getKeywordSet() {
    return new HashSet<String>(Arrays.asList(keywords.split(" ")));
  }
  public String getKeywords() {
    return keywords;
  }
  public String getName() {
    return name.replace(".wiki", "").substring(name.lastIndexOf("/") + 1)
        .replace("_", " ");
  }
  public String getFullName() {
    return name;
  }
  public String getPackageName() {
    return (name.lastIndexOf("/") < 0) ? "/" : name.substring(0,
        name.lastIndexOf("/"));
  }
  public String getPermission() {
    return permission;
  }
  public String getSourceName() {
    return sourceName;
  }
  public String getSourceInfo() {
    return PageStore.getRawWikiLink(sourceName, name);
  }
  public String getLink() {
    return PageStore.getEncodedWikiLink(sourceName, name);
  }

  Document getSearchDoc() throws Exception {
    Document doc = new Document();
    FieldType ft = new FieldType();
    ft.setIndexed(true);
    Field titleField = new Field("title", getName(), new FieldType(ft));
    titleField.setBoost(3);
    doc.add(titleField);
    if (getKeywords() != null) {
      Field keywordField = new Field("keywords", getKeywords(), new FieldType(
          ft));
      keywordField.setBoost(2);
      doc.add(keywordField);
    }
    Field contentField = new Field("content", getContent(), new FieldType(ft));
    contentField.setBoost(1);
    doc.add(contentField);
    return doc;
  }

}
