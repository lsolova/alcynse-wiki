package org.alcynse.wiki.page;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

/**
 * The page indexer thread.
 * 
 * @author Laszlo Solova
 */
public class PageStoreIndexer extends Thread {

  private List<WikiPage> pagesToIndex;
  private RAMDirectory   indexDirectory;
  private IndexReader    indexReader;
  private int            allCount     = 0;
  private int            successCount = 0;

  public PageStoreIndexer(Collection<WikiPage> indexablePages) {
    indexDirectory = new RAMDirectory();
    pagesToIndex = new ArrayList<>(indexablePages);
    allCount = pagesToIndex.size();
    this.setDaemon(true);
  }

  public void addIndexablePages(List<WikiPage> indexablePages) {
    pagesToIndex.addAll(indexablePages);
    allCount = pagesToIndex.size();
    successCount = 0;
    synchronized (this) {
      notify();
    }
  }

  @Override
  public void run() {
    while (true) {
      if( pagesToIndex.isEmpty() )
      {
        synchronized (this) {
          try {
            wait();
          } catch (InterruptedException e) {
            // Do nothing
          }
        }
      }
      if( !pagesToIndex.isEmpty() )
      {
        try {
          IndexWriter iW = new IndexWriter(indexDirectory,
              new IndexWriterConfig(Version.LUCENE_40, new StandardAnalyzer(
                  Version.LUCENE_40)));
          Iterator<WikiPage> wPageIt = pagesToIndex.iterator();
          while (wPageIt.hasNext()) {
            WikiPage wPage = wPageIt.next();
            try {
              String wPageLink = wPage.getLink();
              iW.deleteDocuments(new TermQuery(new Term("pageid", wPageLink)));
              Document searchDoc = wPage.getSearchDoc();
              FieldType ft = new FieldType();
              ft.setTokenized(false);
              ft.setStored(true);
              searchDoc.add(new Field("pageid", wPageLink, ft));
              iW.addDocument(searchDoc);
              wPageIt.remove();
              successCount++;
              System.out.println(getSuccessPercent());
            } catch (Exception e) {
              // TODO: handle exception
              e.printStackTrace();
            }
          }
          iW.commit();
          iW.close();
          indexReader = DirectoryReader.open(indexDirectory);
        } catch (Exception e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
      }
    }
  }

  public IndexReader getIndexReader() {
    return indexReader;
  }
  public int getSuccessPercent() {
    return (int) (((double) successCount / allCount) * 100);
  }

}
