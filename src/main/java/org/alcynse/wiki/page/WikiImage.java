package org.alcynse.wiki.page;

public class WikiImage {
  private String sourceName;
  private String name;

  public WikiImage(String sourceName, String name) {
    this.sourceName = sourceName;
    this.name = name;
  }
  public String getName() {
    return name;
  }
  public String getSourceName() {
    return sourceName;
  }
  public void setName(String name) {
    this.name = name;
  }
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

}
