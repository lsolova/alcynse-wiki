package org.alcynse.wiki.page;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.alcynse.wiki.AlcynseWikiServlet;
import org.slf4j.LoggerFactory;
import org.xwiki.component.embed.EmbeddableComponentManager;
import org.xwiki.rendering.block.Block;
import org.xwiki.rendering.block.ImageBlock;
import org.xwiki.rendering.block.LinkBlock;
import org.xwiki.rendering.block.SpaceBlock;
import org.xwiki.rendering.block.SpecialSymbolBlock;
import org.xwiki.rendering.block.WordBlock;
import org.xwiki.rendering.block.XDOM;
import org.xwiki.rendering.block.match.ClassBlockMatcher;
import org.xwiki.rendering.listener.reference.ResourceReference;
import org.xwiki.rendering.listener.reference.ResourceType;
import org.xwiki.rendering.parser.Parser;
import org.xwiki.rendering.renderer.BlockRenderer;
import org.xwiki.rendering.renderer.printer.DefaultWikiPrinter;
import org.xwiki.rendering.renderer.printer.WikiPrinter;
import org.xwiki.rendering.syntax.Syntax;

/**
 * This object is the place of wiki page content parsing.
 * 
 * @author Laszlo
 */
public class PageParser {

  /**
   * Parse wiki page into an XHTML string. The links and images are specially
   * prepared.
   * 
   * @param wikiPage The wiki page to prepare.
   * @return The prepared wiki page as XHTML string.
   * @throws Exception If something went wrong.
   */
  public static String parse(WikiPage wikiPage) throws Exception {
    EmbeddableComponentManager cM = new EmbeddableComponentManager();
    cM.initialize(wikiPage.getClass().getClassLoader());
    Parser parser = cM
        .getInstance(Parser.class, Syntax.XWIKI_2_1.toIdString());
    XDOM xdom = parser.parse(new StringReader(wikiPage.getRawContent()));

    // Prepare links
    prepareLink(wikiPage, xdom);

    // Prepare images
    prepareImage(wikiPage, xdom);

    WikiPrinter wikiPrinter = new DefaultWikiPrinter();
    BlockRenderer renderer = cM.getInstance(BlockRenderer.class,
        Syntax.XHTML_1_0.toIdString());
    renderer.render(xdom, wikiPrinter);
    return wikiPrinter.toString();
  }

  private static void prepareLink(WikiPage wikiPage, XDOM xdom) {
    for (Block lB : xdom.getBlocks(new ClassBlockMatcher(LinkBlock.class),
        Block.Axes.DESCENDANT)) {

      // Load link block
      if (lB instanceof LinkBlock) {
        ResourceReference linkReference = ((LinkBlock) lB).getReference();
        String url = linkReference.getReference();
        if (url.startsWith("http://") || url.startsWith("https://"))
          continue;
        String text = null;
        List<Block> children = lB.getChildren();
        List<SpecialSymbolBlock> blocksToReplace = new ArrayList<>();
        if (children == null || children.isEmpty()) {
          text = (url.lastIndexOf("/") == -1 ? url : url.substring(url
              .lastIndexOf("/") + 1));
          String[] textBlocks = text.trim().split("_");
          for (int i = 0; i < textBlocks.length; i++) {
            lB.addChild(new WordBlock(textBlocks[i]));
            if (i + 1 < textBlocks.length)
              lB.addChild(new SpaceBlock());
          }
        }
        else {
          StringBuilder textB = new StringBuilder();
          for (Block b : children) {
            if (b instanceof WordBlock) {
              textB.append(((WordBlock) b).getWord());
            }
            else if (b instanceof SpecialSymbolBlock) {
              SpecialSymbolBlock specSymbol = (SpecialSymbolBlock) b;
              textB.append((specSymbol.getSymbol()));
              blocksToReplace.add(specSymbol);
            }
            else if (b instanceof SpaceBlock) {
              textB.append(" ");
            }
          }
          for (SpecialSymbolBlock ssb : blocksToReplace)
            lB.replaceChild(new SpaceBlock(), ssb);

          text = textB.toString();
        }

        try {
          String linkedWikiPageName = PageStore.resolveToAbsolute(
              wikiPage.getPackageName(),
              url.lastIndexOf("/") == -1 ? url : (url.substring(0,
                  url.lastIndexOf("/")))
                  + url.substring(url.lastIndexOf("/")))
                  + ".wiki";
          String encodedWikiLink = PageStore.getEncodedWikiLink(
              wikiPage.getSourceName(), linkedWikiPageName);
          if (PageStore.getWikiPage(encodedWikiLink) != null) {
            String newUrl = "/alcynse-wiki"
                + AlcynseWikiServlet.PATHINFO_PAGE_PREFIX + encodedWikiLink;
            linkReference.setReference(newUrl);
          }
          else {
            LoggerFactory.getLogger(PageParser.class).warn(
                "Page not found: "
                    + PageStore.getRawWikiLink(wikiPage.getSourceName(),
                        linkedWikiPageName));
            lB.setParameter("class", "unknown");
          }
        } catch (Exception e) {
          LoggerFactory.getLogger(PageParser.class).error(
              "Reference setting failed." + url + "|" + text, e);
        }
      }
    }
  }

  private static void prepareImage(WikiPage wikiPage, XDOM xdom) {
    for (Block iB : xdom.getBlocks(new ClassBlockMatcher(ImageBlock.class),
        Block.Axes.DESCENDANT)) {

      // Load image block
      if (iB instanceof ImageBlock) {
        ResourceReference imageReference = ((ImageBlock) iB).getReference();
        if (iB.getParameter("alt") == null
            && iB.getParameter("title") != null)
          iB.setParameter("alt", iB.getParameter("title"));
        String url = imageReference.getReference();
        try {
          // Set up image URL
          String linkedImageName = PageStore.resolveToAbsolute(
              wikiPage.getPackageName(),
              url.lastIndexOf("/") == -1 ? url : (url.substring(0,
                  url.lastIndexOf("/")))
                  + url.substring(url.lastIndexOf("/")));
          String encodedWikiLink = PageStore.getEncodedWikiLink(
              wikiPage.getSourceName(), linkedImageName);
          PageStore.addImage(new WikiImage(wikiPage.getSourceName(),
              linkedImageName));
          String newUrl = "/alcynse-wiki"
              + AlcynseWikiServlet.PATHINFO_IMAGE_PREFIX + encodedWikiLink;
          imageReference.setReference(newUrl);

          // Pack image into a link
          Block parentBlock = iB.getParent();
          if (!(parentBlock instanceof LinkBlock)) {
            List<Block> childrenBlocks = new ArrayList<>();
            childrenBlocks.add(iB);
            ResourceReference reference = new ResourceReference(newUrl,
                ResourceType.URL);
            LinkBlock lB = new LinkBlock(childrenBlocks, reference, false);
            parentBlock.replaceChild(lB, iB);
          }
        } catch (Exception e) {
          LoggerFactory.getLogger(PageParser.class).error(
              "Reference setting failed." + url, e);
        }
        System.out.println(imageReference);
      }
    }
  }

}
