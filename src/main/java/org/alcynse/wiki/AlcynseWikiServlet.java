package org.alcynse.wiki;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.alcynse.util.Resource;
import org.alcynse.util.ResourceResolver;
import org.alcynse.wiki.page.PageStore;
import org.alcynse.wiki.page.WikiImage;
import org.alcynse.wiki.page.WikiPage;
import org.alcynse.wiki.web.History;
import org.alcynse.wiki.web.HistoryItem;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

/**
 * This is the entry point of the Alcynse Wiki engine.
 * 
 * @author Laszlo Solova
 */
public class AlcynseWikiServlet extends HttpServlet {
  private static final long      serialVersionUID      = -4168851505884093437L;

  public static final String     PATHINFO_PAGE_PREFIX  = "/page/";
  public static final String     PATHINFO_IMAGE_PREFIX = "/img/";

  /**
   * The authentication provider to support access control.
   */
  private AuthenticationProvider authProvider;

  private byte[]                 page404Content        = "Not found."
      .getBytes();
  private byte[]                 page403Content        = "Access forbidden."
      .getBytes();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    // Resolve Wiki page
    String pathInfo = req.getPathInfo();
    ServletOutputStream outputStream = resp.getOutputStream();
    if (pathInfo.startsWith(PATHINFO_PAGE_PREFIX)) {
      WikiPage wikiPage = PageStore.resolvePage(pathInfo
          .substring(PATHINFO_PAGE_PREFIX.length()));
      // Check access
      boolean hasAccess = (authProvider == null);
      if (wikiPage != null) {
        String permissionName = wikiPage.getPermission();
        if (authProvider != null)
          hasAccess = authProvider.hasAccessPrivilege(req, permissionName);
      }
      // Build page
      int statusCode = HttpServletResponse.SC_OK;
      byte[] mainContent = preparePage(req, wikiPage);
      if (wikiPage == null) {
        statusCode = HttpServletResponse.SC_NOT_FOUND;
        mainContent = page404Content;
      }
      else if (!hasAccess) {
        statusCode = HttpServletResponse.SC_FORBIDDEN;
        mainContent = page403Content;
      }
      // Write content into response
      resp.setStatus(statusCode);
      resp.setCharacterEncoding("UTF-8");
      resp.setContentType("text/html");
      outputStream.write(mainContent);
    }
    else if (pathInfo.startsWith(PATHINFO_IMAGE_PREFIX)) {
      WikiImage img = PageStore.getImage(pathInfo
          .substring(PATHINFO_IMAGE_PREFIX
              .length()));
      try {
        InputStream is = ResourceResolver.getResourceStream(new Resource(img
            .getSourceName(), img.getName()));
        byte[] buffer = new byte[4096];
        int read = -1;
        while ((read = is.read(buffer)) != -1) {
          outputStream.write(buffer, 0, read);
        }
        outputStream.flush();
        is.close();
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    else {
      getServletContext().getNamedDispatcher("default").forward(req, resp);
    }
  }
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    String defaultPage = config.getInitParameter("defaultPage");
    if (defaultPage != null)
      PageStore.setDefaultPage(defaultPage);
    PageStore.init();
    Velocity.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
    Velocity.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");
    Velocity.setProperty(Velocity.RESOURCE_LOADER, "class");
    Velocity.setProperty("class.resource.loader.class",
        "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
  }

  public void setAuthProvider(AuthenticationProvider authProvider) {
    this.authProvider = authProvider;
  }

  private byte[] preparePage(HttpServletRequest req, WikiPage wikiPage)
      throws IOException {
    if (wikiPage == null)
      return "Alcynse Wiki Test Page.".getBytes();
    VelocityContext vContext = new VelocityContext();
    vContext.put("history", History.getHistoryItems(req));
    vContext.put("page", wikiPage);
    Template frameTemplate = Velocity
        .getTemplate("org/alcynse/wiki/templates/frame.vm");
    History.add(req, new HistoryItem(wikiPage.getName(), wikiPage.getLink()));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    Writer wr = new OutputStreamWriter(baos);
    frameTemplate.merge(vContext, wr);
    wr.flush();
    wr.close();
    return baos.toByteArray();
  }

}
