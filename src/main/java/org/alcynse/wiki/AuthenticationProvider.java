package org.alcynse.wiki;

import javax.servlet.http.HttpServletRequest;

/**
 * This authentication provider can check access rights to the actual page.
 * 
 * @author Laszlo Solova
 */
public interface AuthenticationProvider {
  /**
   * On a request this method have to decide if a user has access to a page or
   * not.
   * 
   * @param request The current request object.
   * @param permissionName The name of the permission required to display the
   *          page.
   * @return True, if the user access granted.
   */
  public boolean hasAccessPrivilege(HttpServletRequest request,
      String permissionName);
}
