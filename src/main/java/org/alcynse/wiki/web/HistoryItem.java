package org.alcynse.wiki.web;

import java.io.Serializable;

public class HistoryItem implements Serializable {
  private static final long serialVersionUID = 4250833476766649189L;

  private String name;
  private String link;

  public HistoryItem(String name, String link) {
    this.name = name;
    this.link = link;
  }

  public String getLink() {
    return link;
  }
  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof HistoryItem
        && ((HistoryItem) obj).getLink().equals(getLink());
  }

  @Override
  public int hashCode() {
    return link.hashCode();
  }

  @Override
  public String toString() {
    return name + "|" + link;
  }

}
