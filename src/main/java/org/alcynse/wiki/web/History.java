package org.alcynse.wiki.web;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class History {
  public static void add(HttpServletRequest req, HistoryItem item) {
    List<HistoryItem> history = (List<HistoryItem>) req.getSession(true)
        .getAttribute("userHistory");
    if (history == null) {
      history = new LinkedList<HistoryItem>();
      req.getSession(true).setAttribute("userHistory", history);
    }
    if (history.contains(item)) {
      int itemIndex = history.indexOf(item);
      while (itemIndex + 1 < history.size())
        history.remove(itemIndex + 1);
    }
    else {
      history.add(item);
    }
    if (5 < history.size())
      history.remove(0);
  }
  public static List<HistoryItem> getHistoryItems(HttpServletRequest req) {
    List<HistoryItem> history = (List<HistoryItem>) req.getSession(true)
        .getAttribute("userHistory");
    return history == null ? new ArrayList<HistoryItem>() : history;
  }

}
