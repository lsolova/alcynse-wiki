package org.alcynse.wiki.page;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Laszlo Solova
 */
public class WikiPageTest {

  @Test
  public void createWikiPage() throws Exception {
    WikiPage wikiPage = new WikiPage("test", "test",
        "@permission(\"wikiaccess\")\n\nTest page content.");// \n\n[[/org/alcynse/wiki/page/Simple_test|Simple_test]]");
    Assert.assertEquals("wikiaccess", wikiPage.getPermission());
    Assert.assertEquals("<p>Test page content.</p>", wikiPage.getContent());
  }

}
