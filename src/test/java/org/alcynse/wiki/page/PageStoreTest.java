package org.alcynse.wiki.page;

import junit.framework.Assert;
import org.junit.Test;

/**
 * @author Laszlo Solova
 */
public class PageStoreTest {

  @Test
  public void initPageStore() {
    PageStore.init();
    System.out.println(PageStore.getWikiPageList());
  }

  @Test
  public void resolveToAbsoluteTest() {
    String wikiPath1 = "/org/alcynse/wiki";
    String resolvePath1 = "../wiki2";
    String resolvedPath1 = "/org/alcynse/wiki2";
    String resolvePath2 = "page";
    String resolvedPath2 = "/org/alcynse/wiki/page";
    String resolvePath3 = "../../..";
    String resolvedPath3 = "/";

    Assert.assertEquals(wikiPath1,
        PageStore.resolveToAbsolute(wikiPath1, wikiPath1));
    Assert.assertEquals(resolvedPath1,
        PageStore.resolveToAbsolute(wikiPath1, resolvePath1));
    Assert.assertEquals(resolvedPath2,
        PageStore.resolveToAbsolute(wikiPath1, resolvePath2));
    Assert.assertEquals(resolvedPath3,
        PageStore.resolveToAbsolute(wikiPath1, resolvePath3));
  }

}
